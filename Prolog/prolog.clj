(ns prolog)

(defn variable? [var]
  (and (symbol? var) (= (first (name var)) \?)))

(defn unify [x y bindings]
  (cond (= bindings 'unification-fail) 'unification-fail
        (= x y) bindings
        (variable? x) (unify-single x y bindings)
        (variable? y) (unify-single y x bindings)
        (and (list? x) (list? y)) (unify (rest x) (rest y)
                                         (unify (first x) (first y) bindings))
        :else 'unification-fail))

(defn occurs? [var x bindings]
  (let [x-bind (bindings x)]
    (cond (= var x) true
          (and (variable? x) x-bind) (occurs? var x-bind bindings)
          (list? x) (or (occurs? var (first x) bindings)
                        (occurs? var (rest x) bindings))
          :else false)))

(defn unify-single [var x bindings]
  (let [var-bind (bindings var)
        x-bind (bindings x)]
    (cond var-bind (unify var-bind x bindings)
          (and (variable? x) x-bind) (unify var x-bind bindings)
          (occurs? var x bindings) 'fail
          :else (assoc bindings var x))))

(defn sub-bindings [x bindings]
  (let [x-bind (bindings x)]
    (cond (= bindings 'unification-fail) 'unification-fail
          (nil? bindings) x
          (and (variable? x) x-bind) (sub-bindings x-bind bindings)
          (and (list? x) (seq x)) (cons (sub-bindings (first x) bindings)
                                                 (sub-bindings (rest x) bindings))
          :else x)))

(def rules {'питается '(((питается Петя картофель))
                    ((питается Петя банан))
                    ((питается Вася яблоко))
                    ((питается Маша ?продукт) (питается Петя ?продукт))
                    ((питается Даша картофель))
                    ((питается Даша ?продукт) (питается Вася ?продукт))
                    ((питается ?человек вода)))})

(use 'clojure.walk)

(defn expr-variables [exp]
  (distinct (filter variable? (tree-seq sequential? seq exp))))

(defn rename-vars [x]
  (postwalk-replace (reduce (fn [sub v] (assoc sub v (gensym (name v)))) {} (expr-variables x))
                    x))

(defn infer [goal bindings rules]
  (mapcat (fn [rule] (let [renamed-rule (rename-vars rule)]
              (infer-goals (rest renamed-rule)
                           (unify goal (first renamed-rule) bindings)
                           rules)))
          (rules (first goal))))

(defn infer-goals [goals bindings rules]
  (cond (= bindings 'unification-fail) nil
        (empty? goals) (list bindings)
        :else (mapcat (fn [prev-goal-res] (infer-goals (rest goals) prev-goal-res rules))
                      (infer (first goals) bindings rules))))

(defn solve [goals]
  (let [solutions (infer-goals goals {} rules)]
    (if (empty? solutions)
        (println "No solutions")
        (let [variables (expr-variables goals)]
          (mapcat (fn [var]
                   (let [res (map (fn [bind] (sub-bindings var bind)) solutions)]
                     (println var "=" res)
                     res))
               variables)))))

(solve '((питается Маша ?что) (питается Петя 123)))
(solve '((питается ?кто картофель)))
(solve '((питается Даша ?что)))

(defn main []
  (println "hello"))
;   (println (read)))
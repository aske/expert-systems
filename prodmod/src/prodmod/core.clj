(ns prodmod.core)

(def ^:dynamic *pre-rules* {'give-money '((will-return sensible-amount)),
                            'will-return '((honest has-work)),
                            'honest '((doesnt-lie seems-trustworthy))
                            'sensible-amount '((we-are-rich) (we-are-ok amount-is-ok))})

(defn prepare-rules [pre-rules]
  (reduce-kv (fn [m k v] (assoc m k (map #(set %) v))) {} pre-rules))

(def ^:dynamic *rules* (prepare-rules *pre-rules*))

(def ^:dynamic *questions* {'seems-trustworthy "Do you believe candidate is trustworthy?",
                            'doesnt-lie "Has candidate never lied?",
                            'has-work "Does candidate work?",
                            'we-are-ok "Are we okay money-wise?",
                            'amount-is-ok "Is amount okay?",
                            'we-are-rich "Are we filthy rich?"})

(def ^:dynamic *facts* #{'we-are-rich 'has-work 'doesnt-lie 'seems-trustworthy})

(def ^:dynamic *goal* 'give-money)

(use 'clojure.set)

(defn any-subset? [sets superset]
  (->> sets (map #(clojure.set/subset? % superset)) (some identity)))

(defn infer-forward [facts rules]
  (reduce (fn [new-facts [rule-head rule-body]]
            (if (any-subset? rule-body new-facts)
                (conj new-facts rule-head)
                new-facts))
          facts
          rules))

(defn forward-solve
  ([facts goal] (forward-solve facts goal *rules*))
  ([facts goal rules]
   (if (contains? facts goal)
       true
       (let [new-facts (infer-forward facts rules)]
         (if (= facts new-facts)
             false
             (forward-solve new-facts goal rules))))))
(use 'clojure.math.combinatorics)

(defn infer-backward [goals rules]
  (mapcat (fn [goal]
            (->> goal (replace rules)
                      (map #(if (symbol? %) (list #{%}) %))
                      (apply clojure.math.combinatorics/cartesian-product)
                      (map (fn [maps]
                             (reduce #(clojure.set/union %1 %2) #{} maps)))))
          goals))

(defn backward-solve
  ([facts goal] (backward-solve facts (list #{goal}) *rules*))
  ([facts goals rules]
   (if (any-subset? goals facts)
       true
       (let [new-goals (infer-backward goals rules)]
         (if (= goals new-goals)
             false
             (backward-solve facts new-goals rules))))))

(defn -main []
  (println "Input goal")
  (let [inp-goal (read-line),
        goal (eval (read-string (str "'" inp-goal)))]
    (println "[F]orward or [B]ackward inference?")
    (let [input (read-line),
          facts (reduce-kv (fn [a-facts f q] (println q)
                             (let [ans (read-line)]
                               (if (= ans "yes")
                                   (conj a-facts f)
                                   a-facts)))
                           '()
                           *questions*)]
        (if (= input "f")
            (println (forward-solve (set facts) goal))
            (println (backward-solve (set facts) goal))))))

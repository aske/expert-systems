(def ^:dynamic *pre-rules* {'give-money '((will-return sensible-amount)),
                            'will-return '((honest has-work)),
                            'honest '((doesnt-lie seems-trustworthy))
                            'sensible-amount '((we-are-rich) (we-are-ok amount-is-ok))})

(def ^:dynamic *rules* (prepare-rules *pre-rules*))

(def ^:dynamic *questions* {'seems-trustworhy "Do you believe candidate is trustworthy?",
                            'doesnt-lie "Has candidate lied?",
                            'has-work "Does candidate work?",
                            'we-are-ok "Are we okay money-wise?",
                            'amount-ok "Is amount okay?",
                            'we-are-rich "Are we filthy rich?"})

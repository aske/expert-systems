import Mouse
import Window
import Graphics.Input as Input
import Dict
import Random
import String
import Config
import Util

clearGrey = rgba 111 111 111 0.6

type Point = (Float, Float)

clickLocations : Signal [Point]
clickLocations = foldp (\(x, y) acc -> (toFloat x, toFloat y) :: acc) [] (sampleOn Mouse.clicks Mouse.position)

scene (w,h) locs pos txt (seedXs, seedYs) numClusters seedW =
  let drawCircle p =
          circle 10 |> filled clearGrey
                    |> move (shiftCoords p)
      shiftCoords (x, y) = (x - toFloat w / 2, toFloat h / 2 - y)
      seedCent = zipWith (\x y -> (x * toFloat w, y * toFloat h)) seedXs seedYs
      (c, clusters) = kMeans seedCent locs 
      pathPoints = map shiftCoords c
      txt = show c
      numClusters' = map toFloat [0..numClusters-1]
      
      -- seedWeights' = map (\l -> Dict.fromList <| zip numClusters' l) <| Util.splitList seedCent numClusters
      -- seedWeights' = map (\l -> Dict.fromList <| zip numClusters' l) <| Util.splitList seedWhts' numClusters
      
      -- (fc, fclusterization) = if isEmpty locs then ([], []) else fcMeans seedWeights' locs
      (fc, fclusterization) = if isEmpty locs then ([], []) else fcMeans seedCent locs
      
      kMeansVis = concatMap (\cn -> map (\p -> traced (solid green) 
                                       (path <| map shiftCoords [Util.indexList c cn, p])) (maybe [] id (Dict.lookup cn clusters))) numClusters'
      kMeansVisCenters = map (\center -> circle 5 |> filled red |> move (shiftCoords center)) c
      fcMeansVisCenters = map (\center -> circle 5 |> filled blue |> move (shiftCoords center)) fc
      fcMeansVisStyle w = solid (rgba 0 0 200 w)
      fcMeansVisDegree p ws = map (\wn -> let w = maybe 0 id (Dict.lookup wn ws)
                                         in  traced (fcMeansVisStyle w) (path (map shiftCoords [Util.indexList fc wn, p]))) numClusters'
      fcMeansVis = concat <| zipWith fcMeansVisDegree locs fclusterization
  in  layers <| map (\e -> collage w h e) [fcMeansVis, kMeansVis, kMeansVisCenters, fcMeansVisCenters, map drawCircle locs]

main = scene <~ Window.dimensions ~ clickLocations ~ txt ~ Mouse.position ~ seedCentroids ~ numClustersSignal ~ seedWeights

txt = constant ""

numClustersSignal = constant Config.numClusters

padd (x, y) (x', y') = (x + x', y + y')
pmul (x, y) (x', y') = (x * x', y * y')
psmul k (x, y) = (k * x, k * y)

centroid : [Point] -> Point
centroid points = let (cx, cy) = foldl (\(x, y) (x', y') -> (x + x', y + y')) (0, 0) points 
                      k = toFloat <| length points
                  in (cx / k, cy / k)
                  
fuzzyWeight centroids c point = let dists = map (\cj -> (dist c point / dist cj point) ^ (2 / (Config.fuzzifier - 1))) centroids
                                in  1 / sum dists

fuzzyCentroid : [Dict.Dict Float Float] -> Float -> [Point] -> Point
fuzzyCentroid weights cn points = let weights' = map (\w -> maybe 0 id (Dict.lookup cn w)) weights
                                      wps = zipWith (\w p -> (w ^ Config.fuzzifier) `psmul` p) weights' points
                                      (cx, cy) = foldl1 padd wps
                                      k = sum weights'
                                  in (cx / k, cy / k)
                                  
-- fcMeans : [Dict.Dict Float Float] -> [Point] -> ([Point], [Dict.Dict Float Float])
fcMeans : [Point] -> [Point] -> ([Point], [Dict.Dict Float Float])
-- fcMeans seedCentr points = let go n c weights = let c' = compCentroids weights points
fcMeans seedCentr points = let go n c weights = let c' = compCentroids weights points
                                                    diff = sum <| zipWith (\cp' cp -> sqrt <| distSq cp' cp) c' c
                                                    weights' = newWeights points c'
                                                in  if | n == 0     -> (c, weights) 
                                                       | otherwise -> if | diff <= Config.eps -> (c', weights) 
                                                                        | otherwise         -> go (n-1) c' weights'
                               compCentroids : [Dict.Dict Float Float] -> [Point] -> [Point]
                               compCentroids weights points = map (\cn -> fuzzyCentroid weights cn points) [0..Config.numClusters-1]
                               
                               numClusters' = map toFloat [0..Config.numClusters-1]
                               
                               newWeights : [Point] -> [Point] -> [Dict.Dict Float Float]
                               newWeights points clusters = map (\p -> Dict.fromList <| zip numClusters' (map (\c -> fuzzyWeight clusters c p) clusters)) points
                            in go 1000 seedCentr (newWeights points seedCentr)
                            -- in go 1000 (compCentroids seedWeigs points) seedWeigs
                                    
seedWeights : Signal [Float]
seedWeights = Random.floatList ((*) <~ numClustersSignal ~ (length <~ clickLocations))

seedCentroids : Signal ([Float], [Float])
seedCentroids = (,) <~ Random.floatList numClustersSignal ~ Random.floatList numClustersSignal

distSq : Point -> Point -> Float
distSq (x, y) (x', y') = (x - x')^2 + (y - y')^2

dist p1 p2 = sqrt (distSq p1 p2)

clusterDist : [Point] -> Point -> [Float]
clusterDist centroids (x, y) = map (\cp -> distSq (x, y) cp) centroids

clusterize : [Point] -> [Point] -> [Float]
clusterize centroids points = let distances = map (\p -> clusterDist centroids p) points
                              in map (\d -> Util.funIndex minimum d) distances
                             
clusterizeDict : [Point] -> [Point] -> Dict.Dict Float [Point]
clusterizeDict centroids points = let pc = clusterize centroids points
                                      gather i = justs <| zipWith (\c p -> if c == i then Just p else Nothing) pc points
                                  in  Dict.fromList <| map (\i -> (i, gather i)) [0..Config.numClusters-1]
                                  
kMeans : [Point] -> [Point] -> ([Point], Dict.Dict Float [Point])
kMeans seed points = let go n c clusters = let c' = shift c clusters
                                               diff = sum <| zipWith (\cp' cp -> sqrt <| distSq cp' cp) c' c
                                               clusters' = clusterizeDict c' points
                                           in  if | n == 0 ->  (c, clusters) 
                                                  | otherwise ->  if | diff <= Config.eps -> (c', clusters) 
                                                                    | otherwise         -> go (n-1) c' clusters'
                         shift : [Point] -> Dict.Dict Float [Point] -> [Point]
                         shift centroids clusters = map (\c -> maybe (0, 0) centroid (Dict.lookup c clusters)) [0..Config.numClusters-1]

                in go 1000 seed (clusterizeDict seed points)

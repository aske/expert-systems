import Mouse
import Window
import Graphics.Input as Input
import Dict
import Random
import String

clearGrey = rgba 111 111 111 0.6

type Point = (Float, Float)

clickLocations : Signal [Point]
clickLocations = foldp (\(x, y) acc -> (toFloat x, toFloat y) :: acc) [] (sampleOn Mouse.clicks Mouse.position)

scene (w,h) locs pos txt (seedXs, seedYs) seedW =
  let drawCircle p =
          circle 10 |> filled clearGrey
                    |> move (shiftCoords p)
      shiftCoords (x, y) = (x - toFloat w / 2, toFloat h / 2 - y)
      seedCent = zipWith (\x y -> (x * toFloat w, y * toFloat h)) seedXs seedYs
      (c, clusters) = kMeans seedCent locs 
      pathPoints = map shiftCoords c
      txt = show c
      numClusters' = map toFloat [0..numClusters-1]
      
      -- spl = splitList seedW numClusters
      -- startWeights = map (\dl -> Dict.fromList (zip dl numClusters')) spl
      -- startWeights = let go weights splitWeights = if isEmpty weights 
      --                                              then splitWeights
      --                                              else go (drop numClusters weights) (splitWeights ++ [Dict.fromList <| zip (take numClusters weights) numClusters'])
      --                in  go (take numClusters seedW) [Dict.fromList <| zip (drop numClusters seedW) numClusters']
      -- (fc, fclusterization) = fcMeans startWeights locs
  in  layers [ collage w h (map drawCircle locs)
             , (plainText (show pos)) `beside` (plainText (show seedCent)) `beside` (plainText txt)
             , collage w h [traced (solid red) (path pathPoints)]
             ]

main = scene <~ Window.dimensions ~ clickLocations ~ txt ~ Mouse.position ~ seedCentroids ~ numClustersSignal

splitList l n = let go list acc = if isEmpty list
                                  then acc
                                  else go (drop n list) (acc ++ (take n list))
                in  go l []

-- txt = show <~ (kMeans <~ clickLocations)
txt = constant ""
-- txt = show <~ seedWeights

numClusters = 3
eps = 0.001

fuzzifier = 2

numClustersSignal = constant numClusters
-- numClustersSignal = ((maybe 2 id . String.toInt) <~ content)

stuff = show <| length <~ clickLocations

padd (x, y) (x', y') = (x + x', y + y')
pmul (x, y) (x', y') = (x * x', y * y')
psmul k (x, y) = (k * x, k * y)

centroid : [Point] -> Point
centroid points = let (cx, cy) = foldl (\(x, y) (x', y') -> (x + x', y + y')) (0, 0) points 
                      k = toFloat <| length points
                  in (cx / k, cy / k)
                  
fuzzyWeight centroids c point = let dists = map (\cj -> (dist c point / dist cj point) ^ (2 / (fuzzifier - 1))) centroids
                                in  1 / (sum dists)

fuzzyCentroid' : [Point] -> Point -> [Point] -> Point
fuzzyCentroid' centroids c points = let wp (x, y) = let wk = (fuzzyWeight centroids c (x, y)) ^ fuzzifier in (wk * x, wk * y)
                                        (cx, cy) = foldl1 (\p acc -> acc `padd` wp p) points
                                        k = sum (map (\p -> fuzzyWeight centroids c p) points)
                                   in  (cx / k, cy / k)
                                   
fuzzyCentroid : [Dict.Dict Float Float] -> Float -> [Point] -> Point
fuzzyCentroid weights cn points = let weights' = map (\w -> maybe 0 id (Dict.lookup cn w)) weights
                                      wps = zipWith (\w p -> (w ^ fuzzifier) `psmul` p) weights' points
                                      (cx, cy) = foldl1 (\wp acc -> acc `padd` wp) wps
                                      k = sum weights'
                                  in (cx / k, cy / k)
                                    
fcMeans : [Dict.Dict Float Float] -> [Point] -> ([Point], [Dict.Dict Float Float])
fcMeans seedWeights points = let go n c weights = let c' = compCentroids weights points
                                                      diff = sum <| zipWith (\cp' cp -> sqrt <| distSq cp' cp) c' c
                                                      weights' = newWeights points c'
                                                  in  if n == 0 then (c, weights) 
                                                               else if diff <= eps 
                                                                    then (c', weights) 
                                                                    else go (n-1) c' weights
                                 compCentroids : [Dict.Dict Float Float] -> [Point] -> [Point]
                                 compCentroids weights points = map (\cn -> fuzzyCentroid weights cn points) [0..numClusters-1]
                         
                                 newWeights : [Point] -> [Point] -> [Dict.Dict Float Float]
                                 newWeights points clusters = map (\p -> Dict.fromList <| zip [0..numClusters-1] (map (\c -> fuzzyWeight clusters c p) clusters)) points

                             in let seedCentroids = compCentroids seedWeights points
                                in  go 1000 seedCentroids (newWeights points seedCentroids)
                                    
seedWeights : Signal [Float]
seedWeights = Random.floatList ((*) <~ numClustersSignal ~ (length <~ clickLocations))

seedCentroids : Signal ([Float], [Float])
seedCentroids = (,) <~ Random.floatList numClustersSignal ~ Random.floatList numClustersSignal

distSq : Point -> Point -> Float
distSq (x, y) (x', y') = (x - x')^2 + (y - y')^2

dist p1 p2 = sqrt (distSq p1 p2)

clusterDist : [Point] -> Point -> [Float]
clusterDist centroids (x, y) = map (\cp -> distSq (x, y) cp) centroids

clusterize : [Point] -> [Point] -> [Float]
clusterize centroids points = let distances = map (\p -> clusterDist centroids p) points
                              in map (\d -> funIndex minimum d) distances
                             
clusterizeDict : [Point] -> [Point] -> Dict.Dict Float [Point]
clusterizeDict centroids points = let pc = clusterize centroids points
                                      gather i = justs <| zipWith (\c p -> if c == i then Just p else Nothing) pc points
                                  in  Dict.fromList <| map (\i -> (i, gather i)) [0..numClusters-1]
                                  
kMeans : [Point] -> [Point] -> ([Point], Dict.Dict Float [Point])
kMeans seed points = let go n c clusters = let c' = shift c clusters
                                               diff = sum <| zipWith (\cp' cp -> sqrt <| distSq cp' cp) c' c
                                               clusters' = clusterizeDict c' points
                                           in  if n == 0 then (c, clusters) 
                                               else if diff <= eps 
                                               then (c', clusters) 
                                               else go (n-1) c' clusters'
                         shift : [Point] -> Dict.Dict Float [Point] -> [Point]
                         shift centroids clusters = map (\c -> maybe (0, 0) (\ps -> centroid ps) (Dict.lookup c clusters)) [0..numClusters-1]

                in go 1000 seed (clusterizeDict seed points)

-- don't use with empty lists
elemIndex : a -> [a] -> Float
elemIndex el l = let go i xs = if isEmpty xs 
                               then -1
                               else if head xs == el then i else go (i+1) (tail xs)
                 in  go 0 l
                 
funIndex : ([a] -> a) -> [a] -> Float
funIndex f l = elemIndex (f l) l

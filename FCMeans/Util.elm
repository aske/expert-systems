module Util where
-- disgusting utility functions
-- don't use with empty lists
elemIndex : a -> [a] -> Float
elemIndex el l = let go i xs = if isEmpty xs 
                               then -1
                               else if head xs == el then i else go (i+1) (tail xs)
                 in  go 0 l
                 
funIndex : ([a] -> a) -> [a] -> Float
funIndex f l = elemIndex (f l) l

indexList : [a] -> Float -> a
indexList l n = head <| drop (truncate n) l

splitList : [Float] -> Int -> [[Float]]
splitList l n = let go : [Float] -> [[Float]] -> [[Float]]
                    go list acc = if | isEmpty list -> reverse acc
                                     | otherwise    -> go (drop n list) ((take n list) :: acc)
                in  go l []

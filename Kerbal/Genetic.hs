{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE LambdaCase   #-}
module Genetic where

import           Control.Lens
import           Control.Monad
import           Control.Parallel.Strategies
import           Data.Functor
import           Data.List                   (foldl1', sortBy, subsequences)
import           Data.Vector                 (Vector (..), fromList, (!))
import qualified Data.Vector                 as V (length)
import           Debug.Trace
import           RocketParts
import           RocketScience
import           System.Random.Mersenne
-- wanted to try out impure random-mersenne, now everything looks disgusting, esp. with IO
-- so, if we're stuck with IO, let's go all the way and do MVector

mkInitialPopulation :: Int -> MTGen -> IO [Rocket]
mkInitialPopulation count g = replicateM count (mkRandomRocket g)

evolvePopulation :: Int -> Double -> MTGen -> [Rocket] -> IO [Rocket]
evolvePopulation count rate g pop = do
  let fraction = count `div` 3
      breedFraction = fraction `div` 3
      splitPopulation p fr = (splitAt fr) <$> splitAt fr p
      breedPopulation p1 p2 = concatMap (\(r1, r2) -> [r1, r2]) <$> zipWithM (\r1 r2 -> crossover r1 r2 g) p1 p2
      (highPop, (midPop, _)) = splitPopulation pop fraction
      (highH, (highM, highL)) = splitPopulation highPop breedFraction
  highMut <- mapM (\r -> mutate r rate g) highPop -- 1/3
  midMut <- mapM (\r -> mutate r rate g) midPop -- 1/3
  highHMBreed <- breedPopulation highH highM -- 2/9
  highHHBreed <- breedPopulation highH (reverse highH) -- 2/9
  highHLBreed <- breedPopulation highH highL -- 2/9
  highMidBreed <- breedPopulation highH midPop -- 2/9
  randPop <- mkInitialPopulation fraction g -- 1/3
  return $! concat [ highH, highM
                   , highHMBreed, highHHBreed, highHLBreed, highMidBreed
                   , highMut, midMut, randPop
                   ]

rankPopulation :: [Rocket] -> Double -> Double -> [Rocket]
rankPopulation rockets deltaV cargoMass = sortBy (\r1 r2 -> fitness r2 deltaV cargoMass `compare` fitness r1 deltaV cargoMass) rockets

mkRandomRocket :: MTGen -> IO Rocket
mkRandomRocket g = do
  len <- randomRangeInt g (1, maxStageCount)
  let protoStage = Stage (smallFuels ! 0) (smallEngines ! 0) 2 3 2
  rocket <- replicateM (len+1) (mutateStage 1.0 g protoStage)
  return (rocket, len+1)
  -- return (replicate len protoStage, (1+len))

crossover :: Rocket -> Rocket -> MTGen -> IO (Rocket, Rocket)
crossover r1@(_, l1) r2@(_, l2) g = do
  transplantationPoint <- randomRangeInt g (0, min l1 l2)
  return $! recombine r1 r2 transplantationPoint

-- assumes n is valid
recombine :: Rocket -> Rocket -> Int -> (Rocket, Rocket)
recombine (rocket1, l1) (rocket2, l2) n = ((r1', l2), (r2', l1))
  where
    (r1part1, r1part2) = splitAt n rocket1
    (r2part1, r2part2) = splitAt n rocket2
    !r1' = r1part1 ++ r2part2
    !r2' = r2part1 ++ r1part2

randomRangeInt :: MTGen -> (Int, Int) -> IO Int
randomRangeInt g (a, b) = do
  r <- random g :: IO Int
  return $! a + r `mod` (abs $ b - a)

type Mutation = MTGen -> Stage -> IO Stage

mixMutations :: [Mutation] -> Vector Mutation
mixMutations = fromList . map (foldl1' (\acc f g s -> acc g s >>= f g)) . tail . subsequences

mutations :: Vector Mutation
mutations = mixMutations [  mutateHeight, mutateSideParts, mutateFuelType
                         , mutateEngineType
                         , mutatePartSizes ]
  where
    mutateHeight g s = randomRangeInt g (1, 4) >>= \h -> return $ set height h s
    mutateSideParts g s = randomRangeInt g (0, 4) >>= \sp ->
                          return $ set numSideParts (2*sp) . set numEngines (sp+1) $ s
    -- look for abstract pattern here, maybe in lens
    mutPartType part g s parts = randomRangeInt g (0, V.length parts) >>= \ft ->
                                 return $ set part (parts ! ft) s
    mutPartTypeDiam part g s diam p1 p2 = mutPartType part g s $ case diam of
                                                                    Small -> p1
                                                                    Large -> p2
    mutatePart diam part g s p1 p2 = mutPartTypeDiam part g s diam p1 p2

    mutateFuel diam g s = mutatePart diam fuel g s smallFuels largeFuels
    mutateEngine diam g s = mutatePart diam engine g s smallEngines largeEngines

    mutateFuelType g s = mutateFuel (view (fuel . fuelSize) s) g s
    mutateEngineType g s = mutateEngine (view (engine . engineSize) s) g s
    mutatePartSizes g s = randomRangeInt g (0, 2) >>= \x -> case x of
                                                              0 -> mutateFuel Small g s >>= \s' -> mutateEngine Small g s'
                                                              1 -> mutateFuel Large g s >>= \s' -> mutateEngine Large g s'

selectMutation :: Vector Mutation -> MTGen -> IO Mutation
selectMutation mutations g = (mutations !) <$> randomRangeInt g (0, V.length mutations)

mutateStage :: Double -> MTGen -> Stage -> IO Stage
mutateStage rate g stage = do
        switch <- random g :: IO Double
        if switch <= rate then applyMutation stage else return stage
  where
    applyMutation :: Stage -> IO Stage
    applyMutation stage = do
        mutation <- selectMutation mutations g
        mutation g stage

mutate :: Rocket -> Double -> MTGen -> IO Rocket
mutate (rocket, len) rate g = do
  let stageRate = rate / fromIntegral len
  -- g <- newMTGen Nothing
  rocket' <- mapM (mutateStage stageRate g) rocket
  return (rocket', len)

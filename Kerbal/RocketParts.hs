{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE TemplateHaskell #-}
module RocketParts where

import           Control.Lens
import           Data.Aeson
import           Data.Vector
import           GHC.Generics

-- thedeemon contest
data Diam = Small | Large deriving (Show, Generic)
instance ToJSON Diam

data Fuel = F { _fuelName :: String
              , _mFull    :: Double
              , _mEmpty   :: Double
              , _fuelSize :: Diam
              } deriving (Show, Generic)
makeLenses ''Fuel
instance ToJSON Fuel

smallFuels :: Vector Fuel
smallFuels = fromList [ F "FL-T100 Fuel Tank" 0.5625 0.0625 Small
                      , F "FL-T200 Fuel Tank" 1.125 0.125 Small
                      , F "FL-T400 Fuel Tank" 2.25 0.25 Small
                      , F "FL-T800 Fuel Tank" 4.5 0.5 Small
                      ]

largeFuels :: Vector Fuel
largeFuels = fromList [ F "Rockomax X200-8 Fuel Tank" 4.5 0.5 Large
                      , F "Rockomax X200-16 Fuel Tank" 9 1 Large
                      , F "Rockomax X200-32 Fuel Tank" 18 2 Large
                      , F "Rockomax Jumbo-64 Fuel Tank" 36 4 Large
                      ]

data Engine = E { _engineName   :: String
                , _engineSize   :: Diam
                , _engineMass   :: Double
                , _engineThrust :: Double
                , _engineImpAtm :: Double
                , _engineImpVac :: Double
                } deriving (Show, Generic)
makeLenses ''Engine
instance ToJSON Engine

smallEngines :: Vector Engine
smallEngines = fromList [ E "LV-T30 Liquid Fuel Engine" Small 1.25 215 320 370
                        , E "LV-T45 Liquid Fuel Engine" Small 1.5  200 320 370
                        , E "LV-909 Liquid Fuel Engine" Small 0.5  50  300 390
                        , E "LV-N Atomic Rocket Engine" Small 2.25 60  220 800
                        ]

largeEngines :: Vector Engine
largeEngines = fromList [ E "Rockomax \"Poodle\" Liquid Engine"   Large 2.5 220  270 390
                        , E "Rockomax \"Mainsail\" Liquid Engine" Large 6   1500 280 330
                        , E "Rockomax \"Skipper\" Liquid Engine"  Large 4   650  300 350
                        ]

{-# LANGUAGE BangPatterns #-}
module Main where

import           Control.DeepSeq
import           Control.Lens
import           Data.Aeson
import qualified Data.ByteString.Lazy   as BS
import           Data.List              (dropWhile, group, sort)
import           Data.Vector            ((!))
import           Debug.Trace
import           Genetic
import           RocketParts
import           RocketScience
import           System.Random.Mersenne
import           Text.Printf

s1 = Stage (largeFuels ! 2) (largeEngines ! 1) 0 1 2
s2 = Stage (smallFuels ! 2) (smallEngines ! 0) 2 3 1

evolutionStep :: MTGen -> Int -> [Rocket] -> IO [Rocket]
evolutionStep !g !len !rockets = do
  newPop <- evolvePopulation len mutRate g rockets
  let !ranked = take len $ rankPopulation newPop deltaBudget cargoMass
  return ranked -- trace (" " ++ (show . length $ newPop)) ranked

ppRocket :: Rocket -> String
ppRocket = concatMap ppStage . fst
  where ppStage st = printf "\nHeight: %d, %s\n" (view height st) (show $ view (fuel . fuelSize) st)
                     ++
                     printf ", %d engines, %d side parts" (view numEngines st) (view numSideParts st)
                     ++ "\n"
                     ++ printf "Fuel: %s, engine: %s" (view (fuel . fuelName) st) (view (engine . engineName) st)
                     ++ "\n----------\n"

stepEvol :: Int -> MTGen -> Int -> [Rocket] -> IO ()
stepEvol 0 _ _ rockets = putStrLn . ppRocket $ head rockets
stepEvol !n g initLen !rockets = do
  !a <- evolutionStep g initLen rockets
  -- print $ map (Prelude.length) . group $ (map (\r -> fitness r deltaBudget cargoMass) a)
  let best = fitness (head a) deltaBudget cargoMass
  print $ best
  -- print $ length a
  let nextStep = if best < deltaBudget then stepEvol (n-1) g initLen a else putStrLn . ppRocket $ head a
  if shortCircuit then
     if ((length . map (Prelude.length) . group $ (map (\r -> fitness r deltaBudget cargoMass) a)) > 1)
     then nextStep
     else putStrLn . ppRocket $ head a
  else nextStep

cargoMass = 7
deltaBudget = 12300
mutRate = 0.7
shortCircuit = False
evolPopModifier = 3.5 :: Double

main = do
  -- print $ fitness rocket deltaBudget cargoMass
  g <- newMTGen Nothing
  pop <- mkInitialPopulation 100000 g
  let pop' = takeWhile (\r -> fitness r deltaBudget cargoMass > 0) $ rankPopulation pop deltaBudget cargoMass
  -- print $ map (Prelude.length) . group $ (map (\r -> fitness r deltaBudget cargoMass) pop')
  let best = head pop'
  -- print best
  print $ fitness best deltaBudget cargoMass
  let initLen = round $ evolPopModifier * (fromIntegral $ length pop')
  stepEvol 20 g initLen pop'

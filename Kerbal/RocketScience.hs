{-# LANGUAGE BangPatterns    #-}
{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
module RocketScience where

import           Control.Applicative
import qualified Control.Foldl       as L
import           Control.Lens
import           Data.Aeson
import           Data.Tuple.Select
import           GHC.Generics
import           RocketParts


maxHeight = 7
maxStageCount = 7
gravitationalAcceleration = 9.816
kerbinOrbitDeltaV = 5000.0
kerbinEscapeMinAccel = 15.0
minimumAcceleration = 5.0

type Rocket = ([Stage], Int)

data Stage = Stage { _fuel         :: Fuel
                   , _engine       :: Engine
                   , _numSideParts :: Int
                   , _numEngines   :: Int
                   , _height       :: Int
                   } deriving (Show, Generic)
makeLenses ''Stage

instance ToJSON Stage

fitness :: Rocket -> Double -> Double -> Double
fitness (rocket, len) deltaV cargoMass = let (height, fullMass) = rocketInfo rocket in
  if isValid len height
  -- then maybe 0 (/ deltaV) $ simulateMission rocket (cargoMass + fullMass)
  then maybe 0 id $ simulateMission rocket (cargoMass + fullMass)
  else 0.0

rocketInfo :: [Stage] -> (Int, Double)
rocketInfo = L.fold ((,) <$> foldRecord _height
                         <*> foldRecord stageMassFull)
  where
    foldRecord field = L.Fold (\acc stage -> acc + field stage) 0 id

stageMass :: Stage -> (Fuel -> Double) -> Double
stageMass stage@Stage{..} fuelState = fromIntegral _numEngines * _engineMass _engine
                                    + fuelMass stage fuelState

fuelMass :: Stage -> (Fuel -> Double) -> Double
fuelMass Stage{..} fuelState = fromIntegral (_height * (1 + _numSideParts))
                             * fuelState _fuel

stageMassFull = flip stageMass _mFull
stageMassEmpty = flip stageMass _mEmpty
burntFuelMass stage = fuelMass stage _mFull - fuelMass stage _mEmpty

isValid :: Int -> Int -> Bool
isValid len height = (len <= maxHeight) && (height <= maxStageCount)

simulateMission :: [Stage] -> Double -> Maybe Double
simulateMission (stageOne : rocket) rocketMass = if sufficientAccel then sel2 <$> delta else Nothing
  where
    compDeltaAccel inAtm !acc !mass stage = (checkAccel accel delta', delta', mass')
     where accel = computeAcceleration stage mass
           delta = computeDelta stage mass inAtm
           mass' = mass - stageMassFull stage
           delta' = delta + acc

    firstStageSim = compDeltaAccel True 0 rocketMass stageOne
    missionDeltaAccels = scanl (\(_, dv, m) -> compDeltaAccel False dv m)  firstStageSim rocket
    checkAccel a d = a >= (if d <= kerbinOrbitDeltaV then kerbinEscapeMinAccel else minimumAcceleration)
    -- missionDeltaAccels should always be nonempty
    (delta, sufficientAccel) = L.fold ((,) <$> L.last <*> L.all sel1) missionDeltaAccels

computeDelta :: Stage -> Double -> Bool -> Double
computeDelta s@Stage{..} rocketMass inAtmosphere = tsiolkovskyEq rocketMass rocketMass' impulse
  where
    rocketMass' = rocketMass - burntFuelMass s
    impulse = (if inAtmosphere then _engineImpAtm else _engineImpVac) _engine

computeAcceleration :: Stage -> Double -> Double
computeAcceleration Stage{..} rocketMass =  fromIntegral _numEngines * _engineThrust _engine / rocketMass

tsiolkovskyEq :: Double -> Double -> Double -> Double
tsiolkovskyEq mStart mEnd impulse = log (mStart / mEnd) * impulse * gravitationalAcceleration

{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE RecordWildCards           #-}
module Mamdani where

import           Data.Functor
import           Data.List
import           Graphics.Gnuplot.Simple
import           Text.Read

main = do
  print 1
  -- print SmallThief
  -- let a1 = ((Cond (0.2 `is` SmallThief)) `And` (0.3 `is` KindJudge))
  -- let a2 = (Cond $ 0.9 `is` DangerousThief)
  -- let r1 = ifR a1 Probation
  -- let r2 = ifR a2 Probation
  -- print $ r1
  -- print $ r2
  -- print $ computeCond a1
  -- print $ computeCond a2
  -- print $ computeMamdani [r1]
  -- print $ computeMamdani [r2]
  plotPaths [PNG "term.png"] $ map (\t -> (points $ (membership t))) [Probation .. LongTerm]
  plotPaths [PNG "thief.png"] $ map (\t -> (points $ (membership t))) [SmallThief .. DangerousThief]
  plotPaths [PNG "judge.png"] $ map (\t -> (points $ (membership t))) [KindJudge .. EvilJudge]
  putStrLn "Input thief and judge parameters"
  thief <- (read :: String -> Double) <$> getLine
  judge <- (read :: String -> Double) <$> getLine
  print $ computeMamdani (map (\f -> f thief judge) rules)

data FuzzyFun = FuzzyFun { points  :: [(Double, Double)]
                         , fun     :: Double -> Double
                         , invfuns :: [Double -> Double]
                         }


lineEq (x1, y1) (x2, y2) x = let k = (y2-y1)/(x2-x1)
                             in  k*x + (y2 - k*x2)
invLineEq (x1, y1) (x2, y2) y = let k = (y2-y1)/(x2-x1)
                                in  (y - (y2 - k*x2)) / k

computeCond :: Cond -> Double
computeCond (Cond ant) = computeAntedecent ant
computeCond (And cond ant) = max (computeCond cond) (computeAntedecent ant)
computeCond (Or cond ant) = min (computeCond cond) (computeAntedecent ant)

computeMamdani :: [Rule] -> Double
computeMamdani = aggregate . map (\(If cond (FS _ ff)) -> conclusion (computeCond cond) ff)

instance Show FuzzyFun where
  show FuzzyFun{..} = show points

mkFuzzyFun :: [(Double, Double)] -> FuzzyFun
mkFuzzyFun points = FuzzyFun points fun invFuns
  where
    partFuns = zipWith lineEq points (tail points)
    invFuns  = zipWith invLineEq points (tail points)
    fun = foldl (\acc (fun, (a, _)) -> \x -> if x < a then acc x else fun x)
                (head partFuns)
                (zip (tail partFuns) (tail points))

conclusion :: Double -> FuzzyFun -> FuzzyFun
conclusion cutoff FuzzyFun{..} = mkFuzzyFun $ zip xs ys
  where
    xs = map ($ cutoff) invfuns
    ys = map (min cutoff . fun) xs

aggregate :: [FuzzyFun] -> Double
aggregate fuzzyFuns = centroid
  where
    samplingRate = 100
    a = minimum $ map (minimum . map fst . points) fuzzyFuns
    b = maximum $ map (maximum . map fst . points) fuzzyFuns
    sampleArgs = [a, (a + 1.0/samplingRate) .. b]
    samples = map (maximum . flip map fuzzyFuns . flip fun) sampleArgs
    centroid = sum (zipWith (*) samples sampleArgs) / sum samples

data FuzzySet = FS String FuzzyFun


instance Show FuzzySet where
  show (FS name _) = name

toFS :: (Show a, Fuzzy a) => a -> FuzzySet
toFS s = FS (show s) (membership s)

data Antedecent = Is Double FuzzySet


computeAntedecent :: Antedecent -> Double
computeAntedecent (Is val (FS _ f)) = fun f $ val

is :: (Show a, Fuzzy a) => Double -> a -> Antedecent
val `is` set = Is val (toFS set)

instance Show Antedecent where
  show (Is x s) = "" ++ show x ++ " is " ++ show s ++ ""

data Cond = Cond Antedecent
          | And Cond Antedecent
          | Or Cond Antedecent

class Fuzzy a where
  membership :: a -> FuzzyFun

instance Show Cond where
  show (Cond ant) = show ant
  show (And cond ant) = "(" ++ show cond ++ " and " ++ show ant ++ ")"
  show (Or cond ant) = "(" ++ show cond ++ " or " ++ show ant ++ ")"

data Rule = If Cond FuzzySet

ifR :: (Show a, Fuzzy a) => Cond -> a -> Rule
ifR cond s = If cond (toFS s)

instance Show Rule where
  show (If ant cons) = "If " ++ show ant ++ " then " ++ show cons

data Thief = SmallThief | SeriousThief | DangerousThief deriving (Show, Enum)
data Judge = KindJudge | NormalJudge | EvilJudge deriving (Show, Enum)
data PrisonTerm = Probation | ShortTerm | OrdTerm | LongTerm deriving (Show, Enum)

instance Fuzzy Thief where
  membership SmallThief = mkFuzzyFun [(0, 1), (0.17, 0.5), (0.53, 0)]
  membership SeriousThief = mkFuzzyFun [(0.2, 0), (0.5, 1), (0.7, 0)]
  membership DangerousThief = mkFuzzyFun [(0.6, 0), (0.8, 1), (1, 0)]

instance Fuzzy Judge where
  membership KindJudge = mkFuzzyFun [(0, 0), (0.17, 1), (0.3, 0)]
  membership NormalJudge = mkFuzzyFun [(0.25, 0), (0.5, 1), (0.7, 0)]
  membership EvilJudge = mkFuzzyFun [(0.64, 0), (0.83, 1), (1, 0)]

-- term is 0 to 100
instance Fuzzy PrisonTerm where
  membership Probation = mkFuzzyFun [(0,1), (0.10, 0.41), (0.15, 0)]
  membership ShortTerm = mkFuzzyFun [(0.1, 0), (0.25, 1), (0.45, 0)]
  membership OrdTerm = mkFuzzyFun [(0.4, 0), (0.55, 1), (0.75, 0)]
  membership LongTerm = mkFuzzyFun [(0.7, 0), (0.85, 1), (1, 0)]

rules = [ \x y -> ifR ((Cond (x `is` SmallThief)) `And` (y `is` KindJudge)) Probation
        , \x y -> ifR ((Cond (x `is` SmallThief)) `And` (y `is` NormalJudge)) ShortTerm
        , \x y -> ifR ((Cond (x `is` SmallThief)) `And` (y `is` EvilJudge)) OrdTerm
        , \x y -> ifR ((Cond (x `is` SeriousThief)) `And` (y `is` KindJudge)) ShortTerm
        , \x y -> ifR (Cond (y `is` NormalJudge)) OrdTerm
        , \x y -> ifR (Cond (x `is` DangerousThief)) LongTerm
        ]
